package it.com.burningcode.jira;

import java.util.HashMap;

public class IntegrationTestHelper {
    public static final String FIELD_TYPE_KEY = "com.burningcode.jira.issue.customfields.impl.jira-watcher-field:watcherfieldtype";
    
    public static final String NUMERIC_FIELD_ID = "10201";
    public static final String FIELD_ID = "customfield_" + NUMERIC_FIELD_ID;
    public static final String FIELD_NAME = "My Watchers";
    public static final String FIELD_DESC = "Test watchers field.";
    public static final String FIELD_TYPE = "Watcher Field";
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";
    public static final String BOB_USERNAME = "bob";
    public static final String BOB_PASSWORD = "bob";
    public static final String FRED_USERNAME = "fred";
    public static final String FRED_PASSWORD = "fred";
    public static final String PROJECT_NAME = "Test";
    public static final String PROJECT_KEY = "TST";
    public static final String PROJECT_ID = "10000";

    public static final String EXPORT_WITH_FIELD = "JWF_FieldCreated.zip";
    public static final String EXPORT_WITHOUT_FIELD = "JWF_NoFieldCreated.zip";
    
    public static HashMap<String, String[]> getUsernameFieldMap(String[] usernames) {
    	HashMap<String, String[]> params = new HashMap<String, String[]>();
		params.put(FIELD_ID, usernames);
		return params;
	}
}
