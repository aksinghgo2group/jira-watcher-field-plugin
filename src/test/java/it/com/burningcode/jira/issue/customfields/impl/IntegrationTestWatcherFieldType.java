
package it.com.burningcode.jira.issue.customfields.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.Navigation;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.permission.ProjectPermissions;
import org.junit.Ignore;
import org.xml.sax.SAXException;

import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel.IssueSecurity;
import com.atlassian.jira.functest.framework.admin.IssueSecuritySchemes.IssueSecurityScheme;
import com.atlassian.jira.functest.framework.navigator.ContainsIssueKeysCondition;
import com.atlassian.jira.functest.framework.navigator.GenericQueryCondition;
import com.atlassian.jira.functest.framework.navigator.NavigatorSearch;
import com.atlassian.jira.functest.framework.navigator.SearchResultsCondition;
import com.atlassian.jira.webtests.EmailFuncTestCase;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;

import com.meterware.httpunit.WebForm;

import static it.com.burningcode.jira.IntegrationTestHelper.*;

/**
 * Class used for integration testing for the JIRA Watcher Field Plugin.
 * 
 * TODO Get integration testing on bulk change.
 * TODO Check issue security
 * TODO Write test to check for issue JWFP-9
 * @author Ray Barham
 */
public class IntegrationTestWatcherFieldType extends EmailFuncTestCase {
	private String jiraVersion;

	private void assertIssueExists(String issueKey){
		String issueId = navigation.issue().getId(issueKey).trim();
		assertFalse("Issue doesn't exist.", issueId.isEmpty());
	}
    
    private void assertWatchersNotPresent(String issueKey, String[] watchers){
    	String currentPage = navigation.getCurrentPage();
    	assertIssueExists(issueKey);
    	navigation.issue().gotoIssue(issueKey);
    	
    	tester.clickLink("view-watcher-list");
    	for(String watcher : watchers){
    		tester.assertLinkNotPresent("watcher_link_" + watcher);
    		log.log("Successfully found user " + watcher + " is not a watcher on issue " + issueKey);
    	}
    	navigation.gotoPage(currentPage);
    }
    
    private void assertWatchersPresent(String issueKey, String[] watchers){
    	String currentPage = navigation.getCurrentPage();
    	navigation.issue().gotoIssue(issueKey);
    	
    	tester.clickLink("view-watcher-list");
    	for(String watcher : watchers){
    		tester.assertLinkPresent("watcher_link_" + watcher);
    		log.log("Successfully found user " + watcher + " is a watcher on issue " + issueKey);
    	}
    	navigation.gotoPage(currentPage);
    }

	private void createWatcherField() {
//        administration.customFields().addCustomField(FIELD_TYPE_KEY, FIELD_NAME);
		navigation.gotoAdminSection(Navigation.AdminSection.VIEW_CUSTOM_FIELDS);
		tester.clickLink("add_custom_fields");
	}

	private String createIssue(String projectId, String type, String summary) {
        return createIssue(projectId, type, summary, new HashMap<String, String[]>());
    }

	private String createIssue(String projectId, String type, String summary, Map<String, String[]> params) {
        gotoCreateIssueForm(projectId, type);
        tester.setFormElement("summary", summary);

		for (Object o : params.entrySet()) {
			Map.Entry entry = (Map.Entry) o;
			String[] value = (String[]) entry.getValue();
			tester.setFormElement((String) entry.getKey(), String.join(",", value));
		}
        tester.submit();

		return getIssueKey(new IdLocator(tester, "key-val"));
	}

    private void gotoCreateIssueForm() {
        gotoCreateIssueForm(PROJECT_ID, ISSUE_TYPE_BUG);
    }
    private void gotoCreateIssueForm(String projectId, String issueType) {
        navigation.gotoPage("CreateIssue!default.jspa");
        tester.setFormElement("pid", projectId);
        tester.setFormElement("issuetype", issueType);
        tester.submit("Next");
        tester.submit("Next");
    }

    private String getIssueKey(Locator locator) {
        return locator.getRawText();
    }
    
    private HashMap<String, String[]> getUsernameFieldMap(String[] usernames) {
    	HashMap<String, String[]> params = new HashMap<>();
    	params.put(FIELD_ID, usernames);
    	return params;
    }

    protected WebForm getFormById(WebForm[] forms, String formName) {
        // Loop through the forms till you one w/ the form name
        for(WebForm form : forms){
            if(form.getID().equals(formName)){
                return form;
            }
        }
        fail("No form found with name "+ formName);

        return null;
    }
    
    protected WebForm getFormByName(WebForm[] forms, String formName) {
    	// Loop through the forms till you one w/ the form name 
    	for(WebForm form : forms){
    		if(form.getName().equals(formName)){
    			return form;
    		}
    	}
    	fail("No form found with name "+ formName);

    	return null;    	
    }
    
    private void ignoreWatcherPermissions(boolean value){
    	navigation.gotoPage("/secure/project/EditWatcherFieldSettings!default.jspa");
        tester.setFormElement("ignoreWatcherPermissions", String.valueOf(value));
        tester.submit("Update");
    }

    private void moveIssue(String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText("Move");
        tester.setFormElement("pid", "Test Move");
        tester.clickButton("next_submit");
        tester.submit();
        tester.submit();
        tester.submit();
    }

    private void addUserToGroup(String username, String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.clickLink("edit_members_of_" + groupName);
        tester.setFormElement("usersToAssignStr", username);
        tester.submit("assign");
    }

    private void removeUserFromGroup(String username, String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("editgroups_" + username);
        tester.selectOption("groupsToLeave", groupName);
        tester.clickButton("user-edit-groups-leave");
    }

    @Override
    public void setUpTest() {
        jiraVersion = administration.getEdition();
        administration.restoreData(EXPORT_WITH_FIELD);

        // Seems to be a problem with the imported 'bob' user.  Deleting and re-adding fixes the issue until a new
        // import is created
        administration.usersAndGroups().deleteUser(BOB_USERNAME);
        administration.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);

    	super.setUpTest();
    }
    
    @Override
    public void tearDownTest() {
    }

    private WebForm setWatcherFieldForm(WebForm[] forms, String values){
    	return setWatcherFieldForm(forms, values, null);
    }
    
    private WebForm setWatcherFieldForm(WebForm[] forms, String values, String expectedExistingValues){
    	// Loop through the forms till you one w/ the watcher field is found 
    	for(WebForm form : forms){	
    		if(form.hasParameterNamed(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID)){
    			if(expectedExistingValues != null){
    				tester.assertFormElementEquals(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID, expectedExistingValues);
    			}
    			form.setParameter(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID, values);
    			return form;
    		}
    	}
    	fail("No form found with watcher field ID "+ it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID);

    	return null;
    }
    
    /**
     * Test adding a watcher field to JIRA.
     */
    public void testCreateWatcherField() {
    	log.log("### Test creating watcher field ###");

    	administration.restoreData(EXPORT_WITHOUT_FIELD);

    	navigation.gotoCustomFields();

//    	if(jiraVersion.equals("4.3")){
//			tester.assertTextNotInTable("custom-fields", FIELD_NAME);
//			tester.assertTextNotInTable("custom-fields", FIELD_TYPE);
//    	}else{
    		tester.assertTableNotPresent("custom-fields");
//    	}
		createWatcherField();
		assertTrue(true);

//        tester.assertTextInTable("custom-fields", FIELD_NAME);
//        tester.assertTextInTable("custom-fields", FIELD_TYPE);
    }
    
    /**
     * Test deleting a watcher field from JIRA.
     */
    public void testDeleteWatcherField() {
    	log.log("### Test delete watcher field ###");
    	
    	navigation.gotoCustomFields();
    	tester.assertTextInTable("custom-fields", FIELD_NAME);
    	tester.assertTextInTable("custom-fields", FIELD_TYPE);
    	administration.customFields().removeCustomField(FIELD_ID);
    	
    	if(jiraVersion.equals("4.3")){
			tester.assertTextNotInTable("custom-fields", FIELD_NAME);
			tester.assertTextNotInTable("custom-fields", FIELD_TYPE);
    	}else{
    		tester.assertTableNotPresent("custom-fields");
    	}
    }
    
    /**
     * Test adding watchers via the watcher field on issue create.
     */
    public void testAddWatcherOnIssueCreate() {
    	log.log("### Test adding watcher on issue create ###");
    	
    	HashMap<String, String[]> params = getUsernameFieldMap(new String[]{BOB_USERNAME});
		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create", params);
    	navigation.issue().gotoIssue(issueKey);
    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME});
    }

    /**
     * Test adding watchers via the watcher field on issue edit.
     * @throws SAXException 
     * @throws IOException 
     */
    public void testAddWatcherOnIssueEdit() throws IOException, SAXException {
    	log.log("### Test adding watcher on issue edit ###");
    	
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue edit.");
    	assertWatchersNotPresent(issueKey, usernames);
    	navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(this.form.getForms(), ADMIN_USERNAME + ", " + BOB_USERNAME).submit();
    	assertWatchersPresent(issueKey, usernames);
    }
    
    /**
     * Test modifying watchers via the watcher field.
     * @throws SAXException 
     * @throws IOException 
     */
    public void testModifyingWatcherOnIssueEdit() throws IOException, SAXException {
    	log.log("### Test modify watcher on issue edit ###");
    	
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};
    	
    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
//    	String issueKey = navigation.issue().createIssue("Test", ISSUE_TYPE_BUG, "Test modify watchers on issue edit", params);
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test modify watchers on issue edit", params);

    	assertWatchersPresent(issueKey, usernames);
    	
    	navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(this.form.getForms(), usernames[0], usernames[0] + ", " + usernames[1]).submit();
    	
    	assertWatchersPresent(issueKey, new String[]{usernames[0]});
    	assertWatchersNotPresent(issueKey, new String[]{usernames[1]});
    }
    
    public void testMovingIssueWithWatchers() throws IOException, SAXException {
    	log.log("### Test moving issues with watchers ###");

        administration.restoreData("test_move.zip");
    	
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
//    	String issueKey = navigation.issue().createIssue("Test", ISSUE_TYPE_BUG, "Test move issue with watchers", params);
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test move issue with watchers", params);

        removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);
        moveIssue(issueKey);
        tester.dumpResponse();
        assertIssueExists("TESTMOVE-1");
    }
    
    /**
     * Test configuring the watcher field.
     */
    public void testConfigureWatcherField() {
    	log.log("### Test configure watcher field ###");

    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test default watchers without configuration.");
    	assertWatchersNotPresent(issueKey, usernames);
    	
    	// Set the default value for the watcher field
    	administration.customFields().setDefaultValue(NUMERIC_FIELD_ID, usernames[1]);
    	
//    	issueKey = navigation.issue().createIssue("Test", ISSUE_TYPE_BUG, "Test default watchers with configuration.");
    	issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test default watchers with configuration.");
    	assertWatchersNotPresent(issueKey, new String[]{usernames[0]});
    	assertWatchersPresent(issueKey, new String[]{usernames[1]});
    }
    
    /**
     * Checks simple filter/searching using the watcher field.  Also checks that issues are being re-indexed on adding watchers (otherwise, searches would not work).
     */
//    public void testSimpleFilterByWatcher() {
//    	log.log("### Test simple filter by watcher ###");
//
//    	String[] usernames = new String[]{BOB_USERNAME};
//
//    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
//    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test simple filter by watcher", params);
//    	assertWatchersPresent(issueKey, usernames);
//
//    	GenericQueryCondition watcherCondition = new GenericQueryCondition(FIELD_ID);
//    	watcherCondition.setQuery(usernames[0]);
//    	NavigatorSearch search = new NavigatorSearch(watcherCondition);
//    	navigation.issueNavigator().createSearch(search);
//    	tester.submit();
//
//    	ArrayList<SearchResultsCondition> searchResultsConditions = new ArrayList<SearchResultsCondition>();
//    	searchResultsConditions.add(new ContainsIssueKeysCondition(text, issueKey));
//    	assertions.getIssueNavigatorAssertions().assertSearchResults(searchResultsConditions);
//    }
    
    /**
     * Checks sql filter/searching using the watcher field
     */
//    public void testJqlFilterByWatcher() {
//    	log.log("### Test jql filter by watcher ###");
//
//    	String[] usernames = new String[]{BOB_USERNAME};
//
//    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
//    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test jql filter by watcher", params);
//    	assertWatchersPresent(issueKey, usernames);
//
//    	navigation.issueNavigator().createSearch("\"My Watchers\" = " + usernames[0]);
//    	tester.submit();
//
//    	assertions.getIssueNavigatorAssertions().assertNoJqlErrors();
//
//    	ArrayList<SearchResultsCondition> searchResultsConditions = new ArrayList<SearchResultsCondition>();
//    	searchResultsConditions.add(new ContainsIssueKeysCondition(text, issueKey));
//    	assertions.getIssueNavigatorAssertions().assertSearchResults(searchResultsConditions);
//    }

    /**
     * Checks that change history is effected properly.  See issue JWF-5.
     * @throws SAXException
     * @throws IOException
     */
//	public void testChangeHistory() throws IOException, SAXException {
//    	log.log("### Test change history ###");
//
//		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test change history without watchers specified.");
//		navigation.issue().gotoIssueChangeHistory(issueKey);
//
//		// Verify no change history for the watcher field is added on issue create.
//		//tester.assertTextPresent("No changes have yet been made on this issue.");
//
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), BOB_USERNAME + ", " + ADMIN_USERNAME).submit();
//
//		ArrayList<ExpectedChangeHistoryItem> expectedChangeItems = new ArrayList<ExpectedChangeHistoryItem>();
//		expectedChangeItems.add(new ExpectedChangeHistoryItem(FIELD_NAME, "[ None ]", "Administrator, Bob The Builder [ Administrator, Bob The Builder ]"));
//		ExpectedChangeHistoryRecord changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
//
//		// Verify change history when adding watchers
//		assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
//
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), ADMIN_USERNAME).submit();
//
//    	// Verify change history when changing watchers
//    	expectedChangeItems.set(0, new ExpectedChangeHistoryItem(FIELD_NAME, ADMIN_FULLNAME + ", " + BOB_FULLNAME, ADMIN_FULLNAME));
//    	changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
//    	assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
//
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), "").submit();
//
//    	// Verify change history when clearing watchers
//    	expectedChangeItems.set(0, new ExpectedChangeHistoryItem(FIELD_NAME, ADMIN_FULLNAME, "None"));
//    	changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
//    	assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
//    }
	
	/**
     * Checks that change history is correct with invalid user.
     * @throws SAXException
     * @throws IOException
     */
	@SuppressWarnings("EmptyMethod")
	public void testChangeHistoryWithInvalidUsers() throws IOException, SAXException {
//    	log.log("### Test change history ###");    	log.log("### Test change history ###");
//    	
//    	administration.usersAndGroups().removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);
//    	
//    	navigation.issue().goToCreateIssueForm("Test", ISSUE_TYPE_BUG);
//    	
//    	tester.setFormElement("summary", "Test change history with invalid watchers.");

//    	String issueKey = navigation.issue().createIssue("Test", ISSUE_TYPE_BUG, "Test change history with invalid watchers.");
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME).submit();

//		ArrayList<ExpectedChangeHistoryItem> expectedChangeItems = new ArrayList<ExpectedChangeHistoryItem>();
//		expectedChangeItems.add(new ExpectedChangeHistoryItem(FIELD_NAME, "None", ADMIN_FULLNAME));
//		ExpectedChangeHistoryRecord expectedChangeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
		
//    	ExpectedChangeHistoryRecord expectedChangeHistoryRecord = new ExpectedChangeHistoryRecord(new ExpectedChangeHistoryItem(FIELD_NAME, "None", ADMIN_FULLNAME));
    	
		// Verify change history when adding watchers
//		assertions.assertLastChangeHistoryRecords(issueKey, expectedChangeHistoryRecord);
    	
//		navigation.issue().gotoIssueChangeHistory(issueKey);
//		assertions.getTextAssertions().assertTextNotPresent("Administrator, Bob The Builder");
		
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), FIELD_ID, ADMIN_USERNAME).submit();
//    	
//    	// Verify change history when changing watchers
//    	expectedChangeItems.set(0, new ExpectedChangeHistoryItem(FIELD_NAME, ADMIN_FULLNAME + ", " + BOB_FULLNAME, ADMIN_FULLNAME));
//    	changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
//    	assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
//    	
//		navigation.issue().gotoEditIssue(issueKey);
//    	setWatcherFieldForm(this.form.getForms(), FIELD_ID, "").submit();
//    	
//    	// Verify change history when clearing watchers
//    	expectedChangeItems.set(0, new ExpectedChangeHistoryItem(FIELD_NAME, ADMIN_FULLNAME, "None"));
//    	changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItems);
//    	assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
    }
    
    /**
     * Checks that watchers are edited properly on issue transition.  See issue JWF-4.
     * @throws SAXException 
     * @throws IOException 
     */
//    public void testEditWatcherOnIssueTransition() throws IOException, SAXException {
//    	log.log("### Test edit watcher on issue transition ###");
//
//    	// Add the watcher field to the resolve workflow screen
//    	//administration.viewFieldScreens().goTo();
//    	//administration.viewFieldScreens().configureScreen("Workflow Screen");
//    	//tester.selectOption("fieldId", FIELD_NAME);
//    	backdoor.screens().addFieldToScreen(DEFAULT_FIELD_SCREEN_NAME, FIELD_NAME);
//
//    	//tester.submit("Add");
//
//    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test edit watchers on issue transition.");
//    	navigation.issue().closeIssue(issueKey, "Fixed", null);
//    	tester.clickLinkWithText("Reopen Issue");
//    	setWatcherFieldForm(this.form.getForms(), BOB_USERNAME + ", " + ADMIN_USERNAME).submit();
//
//    	// Check that the watchers were successfully added
//    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME, ADMIN_USERNAME});
//    }
    
    /**
     * Verifies that JWFP-13 is resolved
     * 
     * Run using: atlas-debug --jvmargs "-server -Xms1024m -Xmx1024m -XX:PermSize=256m -Datlassian.mail.senddisabled=false -Datlassian.mail.fetchdisabled=false -Datlassian.mail.popdisabled=false -Dmail.debug=true -Dmail.smtp.localhost=true"
     * 
     * @throws InterruptedException
     */
    /* Until services are able to be triggered manually, have to disable this test.
	public void testCreateIssueViaEmail() throws InterruptedException, MessagingException, UnableToAddServiceException, UserException, FolderException {
    	log.log("### Test add watchers on create issue via email ###");

    	// Set the default user for the watcher field
    	administration.customFields().setDefaultValue(NUMERIC_FIELD_ID, BOB_USERNAME);
    	
		assertSendingMailIsEnabled();

		JIRAServerSetup.POP3.setPort(110);
		configureAndStartGreenMail(JIRAServerSetup.ALL);
		getGreenMail().setUser(ADMIN_EMAIL, ADMIN_USERNAME, ADMIN_PASSWORD);

		assertTrue(getGreenMail().getPop3().isAlive());
		assertTrue(getGreenMail().getSmtp().isAlive());
		assertTrue(getGreenMail().getImap().isAlive());
		
		// Setup the mail server in JIRA
		setupJiraImapPopServer();
		setupJiraMailServer(ADMIN_EMAIL, DEFAULT_SUBJECT_PREFIX, String.valueOf(getGreenMail().getSmtp().getPort()));

		// Add service to create issues from POP server
		setupPopService("project=" + PROJECT_KEY + ", issuetype=" + ISSUE_BUG);

        String subject = "This is created by email without watchers";
        String message = "This is the subject.  It is a test subject.";
        
        // Send the message
        GreenMailUtil.sendTextEmail(ADMIN_EMAIL, ADMIN_EMAIL, subject, message, getGreenMail().getSmtp().getServerSetup());
        
		waitForMail(1);
		Thread.sleep(65000);

        // Check that a default watcher was not added with the ignorePermissions set to false
        assertWatchersNotPresent(PROJECT_KEY + "-1", new String[]{BOB_USERNAME});

        // Set the ignorePermissions to true
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");
        tester.setFormElement("ignorePermissions", "true");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
        tester.submit("Update");
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
        
        subject = "This is created by email with watchers";
        message = "This is the subject.  It is a test subject.";
        
        GreenMailUtil.sendTextEmail(ADMIN_EMAIL, ADMIN_EMAIL, subject, message, getGreenMail().getSmtp().getServerSetup());
		
		waitForMail(1);
		Thread.sleep(65000);

        // Check that a default watcher was added with the ignorePermissions set to true
        assertWatchersPresent(PROJECT_KEY + "-2", new String[]{BOB_USERNAME});
    }*/
    
    /*
    public void testBulkEditWatchers() {
        Vector issueList = new Vector();
        issueList.add("TST-1");
        issueList.add("TST-2");
        
        // Bulk edit the watchers on some issues
        displayAllIssues();
        bulkChangeIncludeAllPages();
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        assertFormElementPresent("cbcustomfield_10000");
        selectCheckbox("cbcustomfield_10000");
        //setBulkEditFieldTo("customfield_"+FIELD_ID, "cbcustomfield_"+FIELD_ID);
        //bulkEditOperationDetailsSetAs(easyMapBuild("customfield_"+FIELD_ID, "bob"));
        clickOnNext();
        dumpScreen("screenDump");

        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementNotPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementNotPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");

        // Bulk edit the watchers on some issues
        displayAllIssues();
        clickLink("bulkedit_all");
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        setFormElement("customfield_"+FIELD_ID, "admin");
        clickButton("Next");
        clickButtonWithValue("Confirm");

        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementNotPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementNotPresent("stopwatch_bob");
        
        // Bulk edit the multiple watchers on some issues
        displayAllIssues();
        clickLink("bulkedit_all");
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        setFormElement("customfield_"+FIELD_ID, "admin, bob");
        clickButton("Next");
        clickButtonWithValue("Confirm");
        
        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
    }
    */
    
    public void testNonAdminUserManageWatchersWithoutPermission() {
    	administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
    	
    	navigation.login(FRED_USERNAME, FRED_PASSWORD);

        gotoCreateIssueForm();

    	assertions.getTextAssertions().assertTextPresent("You do not have permission to manage the watcher list.");
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create");
    	assertIssueExists(issueKey);
    }
    
    public void testNonAdminUserManageWatchersWithPermission() {
    	administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);

		backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MANAGE_WATCHERS, JIRA_DEV_GROUP);

    	navigation.login(FRED_USERNAME, FRED_PASSWORD);

        gotoCreateIssueForm();
    	assertions.getTextAssertions().assertTextNotPresent("You do not have permission to manage the watcher list.");
    	HashMap<String, String[]> params = getUsernameFieldMap(new String[]{BOB_USERNAME});
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create", params);
    	assertIssueExists(issueKey);
    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME});
    }
    
    /**
     * Checks that JWFP-22 is resolved
     */
    public void testWatcherFieldPermissions() {
    	log.log("### Test watcher field permissions ###");
    	
    	ignoreWatcherPermissions(false);

//    	administration.usersAndGroups().removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);
        removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);
    	
    	// Check that a user does not get added as a watcher that does not have permission
//    	navigation.issue().goToCreateIssueForm("Test", ISSUE_TYPE_BUG);

        gotoCreateIssueForm();
    	tester.setFormElement("summary", "Test add watchers without permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();


    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

        tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();

        assertions.forms().assertNoErrorsPresent();

        assertWatchersPresent("TST-1", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-1", new String[]{BOB_USERNAME});
    	
    	ignoreWatcherPermissions(true);

//    	navigation.issue().goToCreateIssueForm("Test", ISSUE_TYPE_BUG);
        gotoCreateIssueForm();
    	tester.setFormElement("summary", "Test add watchers ignoring permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();

    	assertions.forms().assertNoErrorsPresent();

        assertWatchersPresent("TST-2", new String[]{BOB_USERNAME, ADMIN_USERNAME});
    	
    	ignoreWatcherPermissions(false);
    	
    	// Check that a user, when giving permission again, can be added as a watcher.
    	administration.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_USERS_GROUP);
        gotoCreateIssueForm();

    	tester.setFormElement("summary", "Test add watchers with permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-3", new String[]{BOB_USERNAME, ADMIN_USERNAME});
    }
    
    public void testWatcherFieldIssueSecurity() {
    	log.log("### Test watcher field with issue security ###");
    	
    	IssueSecurityScheme securityScheme = administration.issueSecuritySchemes().newScheme("Test Security", "");
    	IssueSecurityLevel securityLevel = securityScheme.newLevel("Restricted", "");
    	securityLevel.addIssueSecurity(IssueSecurity.GROUP, JIRA_ADMIN_GROUP);
		backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, JIRA_USERS_GROUP);

    	log.log("### Testing watchers without issue security ###");

        tester.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY + "/issuesecurity");
        tester.clickLink("project-config-issuesecurity-scheme-change");
        tester.setWorkingForm(FunctTestConstants.JIRA_FORM_NAME);
        tester.selectOption("newSchemeId", "Test Security");
        tester.submit("Next >>");
        tester.submit("Associate");

        gotoCreateIssueForm();
        tester.setFormElement("summary", "Test add watchers without issue security permissions");
        tester.setFormElement("security", "10000");
        tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
        tester.submit();

    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

    	tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-1", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-1", new String[]{BOB_USERNAME});
    	
    	log.log("### Set settings to ignore permissions ###");

    	ignoreWatcherPermissions(true);

        HashMap<String, String[]> params = new HashMap<>();
        params.put("security", new String[]{"10000"});
        params.put(FIELD_ID, new String[]{BOB_USERNAME, ADMIN_USERNAME});
        createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers ignoring permissions", params);

    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-2", new String[]{ADMIN_USERNAME, BOB_USERNAME});
    	
    	log.log("### Set settings to not ignore permissions ###");

    	ignoreWatcherPermissions(false);

        gotoCreateIssueForm();
        tester.setFormElement("summary", "Test add watchers without issue security permissions");
        tester.setFormElement("security", "10000");
        tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
        tester.submit();

    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

    	tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();

    	assertWatchersPresent("TST-3", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-3", new String[]{BOB_USERNAME});
    	
    	log.log("### Testing watchers with issue security ###");

        addUserToGroup(BOB_USERNAME, JIRA_ADMIN_GROUP);

        params = new HashMap<>();
        params.put("security", new String[]{"10000"});
        params.put(FIELD_ID, new String[]{BOB_USERNAME, ADMIN_USERNAME});
        createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers ignoring permissions", params);

    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-4", new String[]{ADMIN_USERNAME, BOB_USERNAME});
    }
    
    /**
     * To test for issue #7, https://bitbucket.org/rbarham/jira-watcher-field-plugin/issue/7/null-pointer-exception-when-moving-issue
     */
    public void testWorkflowTransition() {
    	log.log("### Test worlflow transition ###");
    	
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test workflow transition.");
    	assertWatchersNotPresent(issueKey, usernames);

    }
}
